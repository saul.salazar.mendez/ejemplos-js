let item = document.querySelector('#file');

function printDate(date) {
    let hora = '00' + date.getHours();
    const min = '00' + date.getMinutes();
    const sec = '00' + date.getSeconds();
    const mil = '000' + date.getMilliseconds();
    if (hora.substr(-2) == '00') {
        hora = '';
    } else {
        hora = hora.substr(-2) + ':';
    }
    return hora + min.substr(-2)+':'+
        sec.substr(-2)+'.'+mil.substr(-3);
}

function construirVtt(subs) {
    let file = 'WEBVTT\n\n';
    for (sub of subs) {
        file += printDate(sub.tiempoInicio)
            + ' --> ' +
            printDate(sub.timepoFin) + '\n'
            + sub.texto;
    }
    return file;
}

function cargarSubtitulos(fileText) {
    const subtitulo = document.querySelector('#subtitulos');
    const blob = new Blob([fileText], {type: 'text/vtt'});    
    subtitulo.src = URL.createObjectURL(blob);    
}

function abrir(evt) {
    console.log(evt);
    let reader = new FileReader();
    reader.onload = () =>{
        const lines = reader.result.split('\n');
        let file = "";
        let reg = /\d{2}:\d{2}:\d{2},\d{3} --> \d{2}:\d{2}:\d{2},\d{3}/;
        let incTime = new Date(0,0,0,0,0,15,0);
        let inc = incTime.getMilliseconds() + incTime.getSeconds()*1000 + incTime.getMinutes()*60*1000 + incTime.getHours()*60*60*1000;
        let anterior = '';
        let subs = [];
        let contSubs = -1;
        for (let line of lines) {            
            if (reg.test(line)){
                let items = line.split(' --> ');
                const strTime1 = items[0].split(/:|,/);
                const strTime2 = items[1].split(/:|,/);
                let time1 = new Date(0,0,0,strTime1[0], strTime1[1], strTime1[2], strTime1[3]);
                let time2 = new Date(0,0,0,strTime2[0], strTime2[1], strTime2[2], strTime2[3]);
                // let incremento1 = new Date(time1.getTime() - inc);
                // let incremento2 = new Date(time2.getTime() - inc);
                // file += printDate(incremento1) + ' --> '  + printDate(incremento2) + '\n';
                if (contSubs >= 0) {                    
                    let contenido = subs[contSubs].texto.split('\n');
                    let tem = '';
                    for (let i=0; i<contenido.length-2; i++) { tem += contenido[i] + '\n'; }
                    subs[contSubs].texto = tem;
                }
                subs.push({tiempoInicio: time1, timepoFin: time2, texto: ''});
                contSubs ++;
            } else {
                // file += line + '\n';
                // anterior = line;                
                if (contSubs >= 0) {
                    subs[contSubs].texto += line + '\n';
                }
            }
        }        
        cargarSubtitulos( construirVtt(subs) );
    }
    reader.readAsText(evt.target.files[0]);    
}


item.addEventListener('change', abrir)