0
00:00:00,000 --> 00:00:00,000
by RentAnAdviser.com

1
00:01:21,062 --> 00:01:26,361
Hello It is me

2
00:01:26,565 --> 00:01:32,564
I was wondering if after all these
years You had like to met

3
00:01:32,768 --> 00:01:37,967
To go over everything

4
00:01:38,161 --> 00:01:41,760
They say that time′s supposed
to heal ya

5
00:01:41,952 --> 00:01:44,751
But I ain′t done much healing

6
00:01:44,951 --> 00:01:50,550
Hello can you hear me

7
00:01:50,756 --> 00:01:56,455
I am in California dreaming
about who we used to be

8
00:01:56,665 --> 00:02:02,664
When we were younger and free

9
00:02:02,862 --> 00:02:08,561
I have forgotten how it felt before
the world fell at our feet

10
00:02:08,765 --> 00:02:14,764
There′s such a difference between us

11
00:02:14,962 --> 00:02:21,761
And a million miles

12
00:02:21,955 --> 00:02:27,554
Hello from the other side

13
00:02:27,756 --> 00:02:33,055
I must have called a thousand times

14
00:02:33,267 --> 00:02:38,766
To tell you I am sorry for everything
that I have done

15
00:02:38,956 --> 00:02:46,255
But when I call you never
seem to be home

16
00:02:46,463 --> 00:02:51,662
Hello from the outside

17
00:02:51,864 --> 00:02:57,063
At least I can say that I have tried

18
00:02:57,267 --> 00:03:03,066
To tell you I am sorry for
breaking your heart

19
00:03:03,258 --> 00:03:11,257
But it don′t matter. It clearly doesn′t
tear you apart anymore

20
00:03:16,857 --> 00:03:21,756
Hello how are you

21
00:03:21,958 --> 00:03:27,957
It is so typical of me to talk
about myself. I am sorry

22
00:03:28,157 --> 00:03:33,656
I hope that You are well

23
00:03:33,856 --> 00:03:39,655
Did you ever make it out of that town
where nothing ever happened

24
00:03:39,867 --> 00:03:45,966
And It is no secret that
the both of us

25
00:03:46,150 --> 00:03:52,449
Are running out of time

26
00:03:52,651 --> 00:03:58,650
So hello from the other
side other side

27
00:03:58,860 --> 00:04:04,059
I must have called a thousand
times thousand times

28
00:04:04,257 --> 00:04:09,756
To tell you I am sorry for everything
that I have done

29
00:04:09,964 --> 00:04:17,363
But when I call you never
seem to be home

30
00:04:17,567 --> 00:04:23,066
Hello from the outside outside

31
00:04:23,266 --> 00:04:28,465
At least I can say that I have
tried I have tried

32
00:04:28,657 --> 00:04:34,156
To tell you I am sorry for
breaking your heart

33
00:04:34,368 --> 00:04:41,767
But it don′t matter. It clearly doesn′t
tear you apart anymore

34
00:04:41,969 --> 00:04:44,468
Highs highs highs highs

35
00:04:44,664 --> 00:04:47,363
Lows lows lows lows

36
00:04:47,565 --> 00:04:49,464
Anymore

37
00:04:49,668 --> 00:04:50,667
Highs highs highs highs

38
00:04:50,753 --> 00:04:53,252
Lows lows lows lows

39
00:04:53,450 --> 00:04:54,749
Anymore

40
00:04:54,961 --> 00:04:56,860
Highs highs highs highs

41
00:04:57,056 --> 00:04:59,455
Lows lows lows lows

42
00:04:59,649 --> 00:05:00,948
Anymore

43
00:05:01,168 --> 00:05:02,667
Highs highs highs highs

44
00:05:02,853 --> 00:05:04,152
Lows lows lows lows

45
00:05:04,356 --> 00:05:05,855
Anymore

46
00:05:06,051 --> 00:05:11,650
Hello from the other side other side

47
00:05:11,858 --> 00:05:17,057
I must have called a thousand
times thousand times

48
00:05:17,251 --> 00:05:22,950
To tell you I am sorry for everything
that I have done

49
00:05:23,166 --> 00:05:30,065
But when I call you never
seem to be home

50
00:05:30,257 --> 00:05:35,956
Hello from the outside outside

51
00:05:36,168 --> 00:05:41,467
At least I can say that I have
tried I have tried

52
00:05:41,667 --> 00:05:47,166
To tell you I am sorry for
breaking your heart

53
00:05:47,364 --> 00:05:52,363
But it don′t matter. It clearly doesn′t
tear you apart anymore