var app = new Vue({
    el: '#app',
    data: {
      primary: "#1b809e",
      success: "#5cb85c",
      warning: "#f0ad4e",
      danger: "#d9534f",
      default2: "#777777",
      activo: "#ffffff",
      fondo: "#ffffff",
      texto: "#000000"
    },
    methods: {
        submit(e) {
            e.preventDefault();
        },
        change() {
            const div = document.querySelector('.variables');
            div.style.setProperty('--color-import-text', this.activo);
            div.style.setProperty('--color-text', this.texto);
            div.style.setProperty('--color-default', this.default2);
            div.style.setProperty('--color-primary', this.primary);
            div.style.setProperty('--color-success', this.success);
            div.style.setProperty('--color-warning', this.warning);
            div.style.setProperty('--color-danger', this.danger);                        
            div.style.setProperty('--color-background', this.fondo);       
        }
    }
})