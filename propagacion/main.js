function distancia (x,y,x1,y1) {
    return Math.sqrt((x-x1)*(x-x1) + (y-y1)*(y-y1));
}

let canvas = document.querySelector('canvas');
let boton = document.querySelector('#btnInicio');
let progress = document.querySelector('#progressBar');

let lienso = canvas.getContext("2d");
const ancho = 800;
const alto = 300;
const maxPuntosX = 200;
const maxPuntosY = 100;
const incX = ancho/maxPuntosX;
const incY = alto/maxPuntosY;
const maxCiclo = 100;
const probabilidad = {
    arriba:0.4,
    izquierda: 0.4,
    derecha: 0.4,
    abajo: 0.4
};

function getColor(r,g,b) {
    return `rgb(${r},${g},${b})`;
}

let malla = new Array(maxPuntosX);
for (let i = 0; i< maxPuntosX; i++)
{
    malla[i] = new Array(maxPuntosY);
}

class Punto {
    constructor(x,y, color, ciclo) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.ciclo = ciclo;
    }

    draw(lienso) {
        lienso.fillStyle = this.color;
        lienso.fillRect(this.x, this.y, incX, incY);
    }
}

function draw(){
    lienso.clearRect(0,0,500,500);
    for(const lista of malla) {
        for (const punto of lista) {
            if (punto) {
                punto.draw(lienso);
            }
        }
    }
}


canvas.addEventListener('click', (event) => {
    const x = (event.offsetX - event.offsetX%incX) ;
    const y = (event.offsetY - event.offsetY%incY) ;
    malla[x/incX][y/incY] = new Punto(x,y, getColor(0,255,0));
    draw();
});

function start() {
    boton.disabled = true;
    progress.style.width = '0%';
    progress.innerHTML = '0%';
    nextCiclo(0);
}

function nextCiclo(n) {
    progress.style.width = (n+1)*100/maxCiclo+'%';
    progress.innerHTML = ''+(n+1);
    if (n>= maxCiclo) {
        boton.disabled = false;
        progress.style.width = '100%';
        progress.innerHTML = ''+maxCiclo;
    } else {
        const incB = 255 - 255*(n/maxCiclo);
        const incR = 255*(n/maxCiclo);
        analizaMalla(incR, incB, n);
        draw();
        espera(25).then(response => {
            nextCiclo(n+1);
        });
    }
}

function analizaMalla(incR, incB, ciclo) {
    for (let i=0; i<maxPuntosX; i++)
    {
        for (let j=0; j<maxPuntosY; j++) {
            //que tenga un mismo cilco quiere decir que se contagio en en este ciclo por lo que no se debe analisar
            if (malla[i][j] && malla[i][j].ciclo != ciclo) {
                contagioArriba(i,j, incR, incB, ciclo);
                contagioIzquierda(i,j, incR, incB, ciclo);
                contagioDerecha(i,j, incR, incB, ciclo);
                contagioAbajo(i,j, incR, incB, ciclo);
            }
        }
    }
}

function contagioArriba(i,j, incR, incB, ciclo) {
    if(j>0) {        
        if (!malla[i][j-1]){
            meContagio = Math.random() < probabilidad.arriba;        
            if(meContagio){
                malla[i][j-1] = new Punto(i*incX, (j-1)*incY, getColor(incR, 0, incB), ciclo);}
        }
    }
}
function contagioIzquierda(i,j, incR, incB, ciclo) {
    if(i>0) {
        if (!malla[i-1][j]){
            meContagio = Math.random() < probabilidad.izquierda;        
            if(meContagio){
                malla[i-1][j] = new Punto((i-1)*incX, j*incY, getColor(incR, 0, incB), ciclo);}
        }
    }
}
function contagioDerecha(i,j, incR, incB, ciclo) {
    if(i<maxPuntosX-1) {
        if (!malla[i+1][j]){
            meContagio = Math.random() < probabilidad.derecha;
            if(meContagio){
                malla[i+1][j] = new Punto((i+1)*incX, j*incY, getColor(incR, 0, incB), ciclo);}
        }
    }
}
function contagioAbajo(i,j, incR, incB, ciclo) {
    if(j<maxPuntosY-1) {
        if (!malla[i][j+1]){
            meContagio = Math.random() < probabilidad.abajo;
            if(meContagio){
                malla[i][j+1] = new Punto(i*incX, (j+1)*incY, getColor(incR, 0, incB), ciclo);}
        }
    }
}

function espera(n) {
    return new Promise( resolve => {
        setTimeout( () => {
            resolve(true);
        },n);
    } );
}
