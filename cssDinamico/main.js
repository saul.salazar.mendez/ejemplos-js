class Foliadora{
    constructor() {
        this.n = 0;
    }
    get next() {
        return this.n++;
    }
}

const foliadora = new Foliadora();

const plugin = {
    install (Vue, options) {
       Vue.mixin({
            created: function () {
                let css = document.querySelector('#'+this.$cssid);
                if (this.$style && !css) {
                    let style = this.$style.replace(/\n/g, ' ');
                    style = style.replace(/(  )*/g, '');
                    css = document.createElement("style");
                    css.type = "text/css";
                    css.id = this.$cssid;
                    css.innerText = style;
                    document.head.appendChild(css)
                }
            },
            beforeDestroy: function() {
                let mycss = document.querySelector('#'+this.$cssid);                 
                if (mycss) {
                    mycss.parentElement.removeChild(mycss);
                }
            }
        });       
       Vue.prototype.$style = '';
       Vue.prototype.$cssid = 'css'+foliadora.next;
    }
}

Vue.use(plugin);


Vue.component('moco', {
    beforeCreate: function() {        
        this.$style= /*css*/`
        .titulo{
            color: blue;
        }
        `
    },
    template: /*html*/`<h1 class="titulo">MOCO</h1>`,
    methods: {
    }
});



const app = new Vue({  
  methods:{
    data: function () {
        return {
            color: "#red"
        }
    },
    changeColor() {
        this.removeCss();
        this.addCss();
        let body = document.querySelector('body');
        //body.style.setProperty('--background-color', this.color);
    },
    /*addCss() {
        let mycss = document.querySelector('#css1234');        
        if (!mycss) {
            let css = `
            .caca {
                background-color: ${this.color};    
            }
            `
            css = css.replace(/\n/g, ' ');
            mycss = document.createElement("style");
            mycss.type = "text/css";
            mycss.id = "css1234";
            mycss.innerText = css;
            document.head.appendChild(mycss)
        }
    },
    removeCss() {
        let mycss = document.querySelector('#css1234');            
        if (mycss) {
            mycss.parentElement.removeChild(mycss);
            mycss = null;
        }
        
    }*/
  }
}).$mount('#app')