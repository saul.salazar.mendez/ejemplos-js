function distancia (x,y,x1,y1) {
    return Math.sqrt((x-x1)*(x-x1) + (y-y1)*(y-y1));
}

class Item{
    constructor(x,y, ancho, alto, color){
        this.x = x;
        this.y = y;        
        this.alto = alto;
        this.ancho = ancho;
        this.isSelect = false;
        this.color = color
    }

    draw(lienso) {
    }

    inArea(x,y) {
        if (this.x <= x && (this.x+this.ancho) >= x ) {
            if (this.y <= y && (this.y+this.alto) >= y ) {
                return true;
            }
        }
        return false;
    }

    inBolitaRedimencianar(x,y) {
        const px = this.x+this.ancho+10;
        const py = this.y+this.alto+10;
        if ( distancia(px,py, x,y) <10 ) {
            return true;
        }
        return false;
    }

    drawEsquina() {
        lienso.beginPath();
        lienso.fillStyle = 'black';
        lienso.arc(this.x+this.ancho+10, this.y+this.alto+10, 10, 0 , Math.PI*2);
        lienso.fill();
    }

    isOut(x,y){
        if ( (x < this.x-10) || (x> this.x+this.ancho+15)) {
            return true;
        }
        if ( (y < this.y-10) || (y> this.y+this.alto+15)) {
            return true;
        }
        return false;
    }

    incrementoLados(incx, incy){
        this.ancho = this.ancho+incx;
        this.alto = this.alto+incy;
        this.ancho < 0 ?  this.ancho = 10 : '';
        this.alto < 0 ?  this.alto = 10 : '';
    }
}


class Cuadrado extends Item{
    constructor(x,y, ancho, color) {
        super(x,y,ancho,ancho, color);
    }

    draw(lienso) {
        lienso.fillStyle = this.color;
        lienso.fillRect(this.x, this.y, this.ancho, this.alto);
        if (this.isSelect){
            lienso.strokeRect(this.x-10, this.y-10, this.ancho+20, this.alto+20);
            this.drawEsquina();
        }
    }
}

class Texto extends Item{
    constructor(texto, x,y,color) {
        super(x,y,50,30, color);        
        this.texto = texto;
    }

    draw(lienso) {
        lienso.fillStyle = this.color;
        lienso.font = "30px Arial";
        lienso.fillText(this.texto, this.x, this.y+25);        
        if (this.isSelect){
            lienso.strokeRect(this.x, this.y, this.ancho, this.alto);
        }
    }

    incrementoLados(incx, incy){        
    }

    inArea(x,y) {
        if (this.x <= x && (this.x+this.ancho) >= x ) {
            if ( (this.y-25) <= y && (this.y+this.alto) >= y ) {
                return true;
            }
        }
        return false;
    }

    inBolitaRedimencianar(x,y) {        
        return false;
    }

}

class Circulo extends Item{
    constructor(x,y, ancho, color) {
        super(x,y,ancho,ancho, color);
    }

    draw(lienso) {
        if (this.isSelect){
            lienso.beginPath();
            lienso.fillStyle = this.color;
            lienso.arc(this.x+this.ancho/2, this.y+this.ancho/2, this.ancho+10, 0 , Math.PI*2);
            lienso.stroke();
            this.drawEsquina();
        }
        lienso.beginPath();
        lienso.fillStyle = this.color;
        lienso.arc(this.x+this.ancho/2, this.y+this.ancho/2, this.ancho, 0 , Math.PI*2);
        lienso.fill();
    }

    drawEsquina() {
        lienso.beginPath();
        lienso.fillStyle = 'black';
        lienso.arc(this.x+this.ancho+this.ancho/2+10, this.y+this.alto+this.ancho/2+10, 10, 0 , Math.PI*2);
        lienso.fill();
    }

    inBolitaRedimencianar(x,y) {
        const px = this.x+this.ancho+this.ancho/2+10;
        const py = this.y+this.alto+this.ancho/2+10;
        if ( distancia(px,py, x,y) <10 ) {
            return true;
        }
        return false;
    }

    inArea(x,y) {
        if (distancia(this.x+this.ancho/2, this.y+this.ancho/2, x,y) < this.ancho) {
            return true;
        }
        return false;
    }

    isOut(x,y){
        if (distancia(this.x+this.ancho/2, this.y+this.ancho/2, x,y) > this.ancho+this.ancho/2+10) {
            return true;
        }
        return false;
    }

    incrementoLados(incx, incy){
        this.ancho = this.ancho+incx;
        this.alto = this.ancho;
        this.ancho < 0 ?  this.ancho = 10 : '';
        this.alto < 0 ?  this.alto = 10 : '';
    }
}


listaObjetos = [new Texto('Chafa paint', 150, 50, 'black')];

let canvas = document.querySelector('canvas');

let lienso = canvas.getContext("2d");

function draw(){
    lienso.clearRect(0,0,500,500);
    for(const item of listaObjetos) {
        item.draw(lienso);
    }
}

function unSelectItems(){
    for(const item of listaObjetos) {
        item.isSelect = false;
    }
}

function selectItem(x,y) {
    unSelectItems();
    
    for(let i = listaObjetos.length-1; i>=0; i--) {
        const item = listaObjetos[i];
        if (item.inArea(x,y)) {
            item.isSelect = true;
            return item;
        }
    }
    return null;
}

function drawSelected(item, lienso){
    lienso.strokeRect(item.x-10, item.y-10, item.ancho+10, item.alto+10);  
}

draw();

let selectedItem = null;

let selectEsquina = false;

canvas.addEventListener('click', (event) => {
    
});

canvas.addEventListener('mousedown', (event) => {
    const x = event.offsetX;
    const y = event.offsetY;
    if (!selectedItem) {
        selectedItem = selectItem(x,y);
        if (selectedItem) {
            selectedItem.x = x-selectedItem.ancho/2;
            selectedItem.y = y-selectedItem.alto/2;
        }        
    } else if (selectedItem) {        
        selectEsquina = selectedItem.inBolitaRedimencianar(x,y);
    }
    draw();
});

let antx = 0;
let anty = 0;

canvas.addEventListener('mousemove', (event) => {
    const x = event.offsetX;
    const y = event.offsetY;    
    if (event.buttons == 1  && selectedItem){                
        if (selectEsquina) {
            const incx = x-antx;
            const incy = y-anty;
            selectedItem.incrementoLados(incx, incy);
            
        } else {
            selectedItem.x = x-selectedItem.ancho/2;
            selectedItem.y = y-selectedItem.alto/2;
        }        
        draw();
    } else {
        if (selectedItem && selectedItem.isOut(x,y)) {
            selectedItem = null;
            selectEsquina = false;
            unSelectItems();
            draw();
        }
    }
    antx = x;
    anty = y;
});


function addCirculo() {
    const color = 'rgb(' + Math.random()*255 + ', ' +
            Math.random()*255 + ','+ Math.random()*255 +')';
    listaObjetos.push( new Circulo(Math.random()*500,Math.random()*500, Math.random()*150, color));
    draw();
}

function addCuadrado() {
    const color = 'rgb(' + Math.random()*255 + ', ' +
            Math.random()*255 + ','+ Math.random()*255 +')';
    listaObjetos.push( new Cuadrado(Math.random()*500,Math.random()*500, Math.random()*200, color));
    draw();
}

function addTexto() {
    const color = 'rgb(' + Math.random()*255 + ', ' +
            Math.random()*255 + ','+ Math.random()*255 +')';
    let texto = prompt('Ingresa texto', '');
    if (!texto) {
        texto = "Texto ejemplo";
    }
    listaObjetos.push( new Texto( texto, Math.random()*500,Math.random()*500, color));
    draw();
}

function espera() {
    return new Promise( resolve => {
        setTimeout( () => {
            resolve(true);
        }, Math.random()*1000);
    } );
}
function addRandom() {    
    espera().then(response => {
        for (let i = 0; i< 25; i++) {
            espera().then( () => {
                let num = Math.random();
                if (num<0.5) {
                    addCirculo();
                } else {
                    addCuadrado();
                }
            });
        }
    } );
}