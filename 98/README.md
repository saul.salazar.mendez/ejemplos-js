# Link
Son links a aplicaciones(ventanas)
# Ventana
Cada ventana va estar en el escritorio. 
Va tener las siguientes propiedades:
- Titulo
- Icono de titulo
- Botón en barra de estado

Las operaciones que puede hacer con la ventana son:
- Maximizar
- Minimizar
- Cerrar
- Mover
- Cambiar de tamaño

Todas las ventanas estarán en una pila que se utilizara para desplegaras en el escritorio y en la bandeja.

# Escritorio
Es el área donde están las ventanas y los links. Puede definirse un color de fondo o una imagen.
## bandeja de ventanas
Por cada ventana que se cree caerá en la bandeja de ventanas. En esta bandeja se indica cual ventana esta activa. Aquí están las aplicaciones aunque estén minimizadas.