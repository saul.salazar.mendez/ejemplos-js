import { dragAMover, dragSize } from "./dragdrop.js";
import { Foliadora } from "../foliadora.js";
import { listaVentanas } from "./listaVentanas.js";

const style = /*css*/`
.ventana {
    position: absolute;  
}
.resizable {  
    position: absolute;
    right: 3px;
    bottom: 1px;
    color: black;
    font-size: xx-small;
    cursor: se-resize;
  }
.ventana_no_activa {
    background: linear-gradient(90deg,grey,#000000);
}
`;

const template = /*html*/
`
<div class="ventana animate__animated" :id="'ventana__'+id" @click="setActivo()">
    <div class="window" style="width: 300px" draggable="true" @dragstart="drag($event)">
    <div class="title-bar ventana_no_activa" >
        <div class="title-bar-text">{{titulo}}</div>
        <div class="title-bar-controls">
        <button aria-label="Minimize" @click="minimizar()"></button>
        <button aria-label="Maximize" @click="maximizar()"></button>
        <button aria-label="Close" @click="elimina()"></button>
        </div>
    </div>
    <div class="window-body">
       <slot></slot>
    </div>   
    </div>        
    <div class='resizable' draggable="true" @dragstart="dragSize($event)">//</div>        
</div>
`;

let foliadora = new Foliadora();
let zIndex = new Foliadora();
zIndex.n = 10;

export default {
    props: ['titulo', 'ventana'],
    template: template,
    data: function(){
        return {
            id: foliadora.next,
            pos: {
                x: '',
                y: '',
                ancho: '',
                alto: ''
            }
        }
    },
    beforeCreate: function(){
        this.$style = style;
    },
    beforeMount: function(){
        listaVentanas.addVentana({id: this.id, titulo: this.titulo, activo: false});              
    },
    mounted: function(){
        let ventana = this.$el;
        ventana.style.top = this.id*10+"px";
        ventana.style.left = this.id*10+"px";
        ventana.style.zIndex = zIndex.next;
    },
    beforeDestroy: function(){
        listaVentanas.removeVentana(this.id);
    },
    methods: {
        minimizar: function() {
            let ventana = this.$el;
            ventana.classList.add('animate__zoomOutDown');
        },
        addInactivo: function(){
            let lista = document.querySelectorAll('.title-bar');
            for (let ventana of lista){
                ventana.classList.add('ventana_no_activa');
            }
        },
        setActivo: function(){
            let ventana = this.$el.querySelector('.title-bar');
            this.addInactivo();
            ventana.classList.remove('ventana_no_activa');
            listaVentanas.setActivo(this.id);
            this.$el.style.zIndex = zIndex.next;
        },
        maximizar: function() {
            let ventana = this.$el;
            let win = ventana.querySelector('.window');                        
            if (win.style.width !== "100%"){
                this.pos.x = ventana.style.left;
                this.pos.y = ventana.style.top;
                this.pos.ancho = win.style.width;
                this.pos.alto = win.style.height;
                ventana.style.top = "";
                ventana.style.left = "";
                win.style.width = "100%";
                ventana.style.width = "100%";
                win.style.height = "calc(100vh - 35px)";
                ventana.classList.add('animate__headShake');
                ventana.classList.remove('animate__rubberBand');
            } else {
                win.style.width = this.pos.ancho;                
                win.style.height = this.pos.alto;
                ventana.style.left = this.pos.x;
                ventana.style.top = this.pos.y;
                ventana.style.width = "";
                ventana.classList.add('animate__rubberBand');
                ventana.classList.remove('animate__headShake');
            }
        },
        elimina: function(){
            console.log(this.$destroy);
            this.$destroy();
            this.$el.remove();
        },
        drag: function(ev) {                      
            dragAMover(ev);
            this.setActivo();
        },
        dragSize: function(ev) {
            dragSize(ev);
        }
    }
}