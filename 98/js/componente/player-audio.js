const template = /*html*/ `
<ventana :titulo="'Player' + titulo">
    <audio class="audio_player" controls style="widthd"></audio>
    <div class="lista_player">
    <ul>
    <li     
    v-for="(item, index) in lista" :key="index" 
    v-bind:class="{activo: item.activo}"
    @click="play(index)">{{item.nombre}}</li>
    </ul>
    </div>
</ventana>
`;

const style = /*css*/`
.lista_player{
    overflow: auto;
    max-height: 90vh;
    background: white;
    border: solid 1px grey;
  }
  
  .lista_player ul {  
    background-color: #f1f1f1;
    list-style-type: none;
    padding: 10px 20px;
    margin: 0;
    padding: 0;
  }
  .lista_player ul li {
    border-left: 5px solid silver;
    border-bottom: 1px solid #7c7c7c;
    left: 0;
    background-color: #f1f1f1;  
    padding: 10px 20px;
    cursor: pointer;
  }
  
  .lista_player ul .activo{
    border-left: 5px solid grey;
    background-color: #f1f1f1;
    list-style-type: none;
    padding: 10px 20px;
    font-weight: bold;
  }
  .audio_player {
    width: 100%;
  }
`;

const lista = [
    {
        nombre: 'Nyan cat',
        src: 'https://ia800501.us.archive.org/33/items/nyannyannyan/NyanCatoriginal.ogg',
        activo: false
    },
    {
        nombre: 'Pink panther',
        src: 'https://ia800902.us.archive.org/23/items/tvtunes_502/Pink%20Panther.ogg',
        activo: false
    },
    {
        nombre: 'Música clasica mix',
        src: 'https://archive.org/download/ClasicMusic_583/07-CAR1.ogg',
        activo: false
    }
]
export default {
    data: function () {
        return {
            lista: lista,
            player: null,
            titulo: ''
        }
    },
    beforeCreate: function(){
        this.$style = style;
    },
    mounted: function(){
        this.player = this.$el.querySelector('audio');
        const that = this;
        let observer = new MutationObserver( ele => {
            that.changeSize(ele);
        });
        observer.observe(this.$el.querySelector('.window'), { attributes: true });

    },
    template: template,
    methods: {
        desactivaTodos() {
            for(let item of this.lista){
                item.activo = false;
            }
        },
        changeSize(ev) {
            let win = ev[0].target;
            let ancho = parseInt(win.style.width);
            let alto = parseInt(win.style.height);
            let lista = win.querySelector('.lista_player');
            const prop = lista.getBoundingClientRect()
            lista.style.height = alto - 100 + 'px';
        },
        play(index) {  
            this.desactivaTodos();
            const item = this.lista[index];
            this.player.pause();
            this.player.src = item.src;                        
            this.player.load();            
            this.player.play();
            this.titulo = ' - '+item.nombre;
            item.activo = true;
        }
    }
}