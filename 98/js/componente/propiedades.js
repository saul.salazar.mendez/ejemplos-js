const template = /*html*/`
<ventana titulo="Propiedades de pantalla">
<fieldset>
  <legend>Wallpaper</legend>
  <div class="field-row">
    <input id="radio13" type="radio" value="ninguno" v-model="opcion"  @click="setImage('')">
    <label for="radio13">Ninguno</label>
    <input type="color" v-model="this.color" @change="cambioColor($event)" v-if="opcion=='ninguno'">
  </div>
  <div class="field-row">
    <input id="radio14" type="radio" value="win98" v-model="opcion" @click="setImage('win98')">
    <label for="radio14">windows 98</label>
  </div>
  <div class="field-row">
    <input id="radio15" type="radio" value="winxp" v-model="opcion" @click="setImage('winxp')">
    <label for="radio15">Windows xp</label>
  </div>
</fieldset>
</ventana>
`;

export default {
    template: template,
    data(){
        return {
            color: '',
            escritorio: null,
            opcion: 'ninguno'
        }
    },
    mounted(){
        this.escritorio = document.getElementById('escritorio');        
        this.color = "#008080";
    },
    methods: {
        cambioColor(ev) {
            this.escritorio.style.background = ev.target.value;
        },
        setImage(img){
            if (img == '') {
                this.escritorio.style.backgroundImage = '';                                
                this.escritorio.style.backgroundRepeat = '';
            } else {
                this.escritorio.style.backgroundImage = `url("./img/${img}.jpg")`;                
                this.escritorio.style.backgroundRepeat = 'round';
            }
        }
    }

}