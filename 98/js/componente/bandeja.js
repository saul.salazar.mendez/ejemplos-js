import { listaVentanas } from "./listaVentanas.js";



const style = /*css*/`
#toolbar{
    background: #c0c0c0;
    height: 35px;
    width: 100%;
    position: fixed;
    bottom: 0;
    left: 0;
    border-top: 2px solid #fff;
}
#toolbar .toolbar-start-menu{
    float: left;
}

#toolbar .start-button{
    background: url(https://win98icons.alexmeub.com/icons/png/windows-0.png) no-repeat 2px center / 24px;
    padding-left: 28px;
    padding-right: 5px;
    height: 29px;
    margin: 2px 3px;
    font-weight: bold;
    border: 2px solid #7c7c7c;
    border-top-color: #fff;
    border-left-color: #fff;
    float: left;
}

#toolbar .start-button:hover{
    background-color: #7c7c7c;
}

#toolbar .toolbar-ventanas{
    float: left;
    border: 2px solid #7c7c7c;
    border-bottom-color: #fff;
    border-right-color: #fff;
    margin-top: 2px;
}

#toolbar .toolbar-separator{
    width: 2px;
    height: 26px;
    margin-top: 2px;
    background: #797979;
    float: left;
    border-right: 1px solid #fff;
}

.ventanaActiva{
    background: #828282;
    
    color: white;
}

.start-menu-azul{    
    bottom: 37px;
    width: 30px;
    height: 200px;
    background: linear-gradient(90deg,#00009e, navy);
    
}
.start-menu{
    width: 150px;
    height: 200px;
    border: solid 1px;
    position: absolute;
    bottom: 37px;
    background: silver;
    display: flex;
    
}
.titulo-start {           
    bottom: 37px;        
}
`;

const template = /*html*/
`
<div id="toolbar">
        <div class="start-menu">
            <div class="start-menu-azul">                
            </div>
            <div class="titulo-start">
                Usuario
            </div>
        </div>
        
        <button class="start-button">
          Start
        </button>            
        
            
        
        <div class="toolbar-separator"></div>
        <div class="toolbar-ventanas">
            <button v-for="(ventana,index) in ventanas" :key="index" @click="show(ventana.id)" 
            v-bind:class="{ ventanaActiva: ventana.activo}">
                {{ventana.titulo}}
            </button>
        </div>
        <hora></hora>
    </div>
`;

import Hora from './hora.js';
export default {
    data: function(){
        return {
            ventanas: listaVentanas.lista
        }
    },
    template: template,
    beforeCreate: function(){
        this.$style = style;        
    },
    methods: {
        show: function(id) {
            let ventana = document.querySelector('#ventana__'+id);
            if (ventana.classList.toString().indexOf('animate__zoomOutDown') >= 0){
                ventana.classList.add('animate__zoomInUp');
                ventana.classList.remove('animate__zoomOutDown');
                listaVentanas.setActivo(id);
            } else {
                ventana.classList.add('animate__zoomOutDown');
                ventana.classList.remove('animate__zoomInUp');
            }
        }
    },
    components: {
        Hora
    }
}