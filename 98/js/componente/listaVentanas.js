class ListaVentanas {
    constructor() {
        this.lista = [];
    }

    addVentana(ventana) {
        this.lista.push(ventana);
    }

    removeVentana(id) {
        for (let i = 0; i < this.lista.length; i++) {
            if (this.lista[i].id == id) {
                this.lista.splice(i,1);
                break;
            }
        }
    }

    setActivo(id) {
        for (let i = 0; i < this.lista.length; i++) {
            if (this.lista[i].id == id) {
                this.lista[i].activo = true;
            } else {
                this.lista[i].activo = false;
            }
        }
    }
}


let lista = new ListaVentanas();

export const listaVentanas = lista;