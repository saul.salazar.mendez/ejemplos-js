let itemAMover = {
    target: null,
    x: 0,
    y: 0
}

let itemSize = {
    target: null,
    x: 0,
    y: 0
}

let tipo = '';

function incremento(ini, fin) {
    return fin-ini;
}

function numero(p) {
    if (p === '') return 0;
    return parseInt(p);
}

export function dropMover(ev) {
    ev.preventDefault();
    const x = incremento(itemAMover.x, ev.x);
    const y = incremento(itemAMover.y, ev.y);          
    itemAMover.target.parentNode.style.left = numero(itemAMover.target.parentNode.style.left) + x + 'px';
    itemAMover.target.parentNode.style.top = numero(itemAMover.target.parentNode.style.top) + y + 'px';          
} 

export function dropSize(ev) {
    ev.preventDefault();    
    const x = incremento(itemSize.x, ev.x);
    const y = incremento(itemSize.y, ev.y);
    const win = itemSize.target.parentNode.querySelector('.window');
    const body = win.querySelector('.window-body');
    let ancho = 0;
    let alto = 0;
    if (win.style.width == "" || win.style.width == "auto") {
        ancho = win.clientWidth;
    } else {
        ancho = numero(win.style.width);
    }
    if (win.style.height == "" || win.style.height == "auto") {
        alto = win.clientHeight;
    } else {
        alto = numero(win.style.height);
    }
    win.style.width = ancho + x + 'px';
    win.style.height = alto + y + 'px';
    body.style.maxHeight = alto + y - 36 + 'px';
    body.style.overflow = 'auto';
    itemSize.x = ev.x;
    itemSize.y = ev.y;
}

export function drop(ev) {
    const data = ev.dataTransfer.getData("tipo");
    if (data == 'size') {        
        dropSize(ev);
    } else if (data == 'mover') {
        dropMover(ev);
    }
    tipo = '';
}

  
export function dragSize(ev) {              
    itemSize.target = ev.target;
    itemSize.x = ev.x;
    itemSize.y = ev.y;
    ev.dataTransfer.setData("tipo", 'size');    
    tipo = 'size';
}

export function allowDrop(ev) {    
    if (tipo == 'size') {        
        dropSize(ev);
    } else {
        ev.preventDefault();
    }    
    
}
  
export function dragAMover(ev) {
    itemAMover.target = ev.target;
    itemAMover.x = ev.x;
    itemAMover.y = ev.y;
    ev.dataTransfer.setData("tipo", 'mover');
    tipo = 'mover';
}