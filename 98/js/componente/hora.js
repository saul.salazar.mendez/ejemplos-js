const style = /*css*/`
.hora {
    display: inline-block;
    float: right;
    text-align: center;
    padding: 4px;
    border-top: 2px solid grey;
    border-left: 2px solid grey;
    border-bottom: 2px solid white;
    border-right: 2px solid white;
    margin-top: 2px;
  }
`

const template = /*html*/ `
<div class="hora">
{{horaActual}}
</div>
`;

export default {
    template: template,
    data: function(){
        return {
            horaActual: '',
            reloj: null
        }        
    },
    beforeCreate: function() {
        this.$style = style;
    },
    mounted: function(){
        const that = this;
        this.reloj = setInterval(() => {            
            let data = new Date();
            let hora = data.getHours();
            let terminal = ' ';
            terminal += hora > 12 ? 'PM' : 'AM';
            hora = hora > 12 ? hora - 12 : hora;
            that.horaActual = hora + ':' + data.getMinutes() 
                + ':' + data.getSeconds() + terminal;            
        }, 2000)
    },
    destroy(){
        clearInterval(this.reloj);
    }
}
