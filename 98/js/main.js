import {Foliadora} from './foliadora.js';

const foliadora = new Foliadora();

const plugin = {
    install (Vue, options) {        
        Vue.mixin({
            created: function () {
                this.$cssid = 'css'+foliadora.next;
                let css = document.querySelector('#'+this.$cssid);
                if (this.$style && !css) {
                    let style = this.$style.replace(/\n/g, ' ');
                    style = style.replace(/(  )*/g, '');
                    css = document.createElement("style");
                    css.type = "text/css";
                    css.id = this.$cssid;
                    css.innerText = style;
                    document.head.appendChild(css)
                }
            },
            beforeDestroy: function() {
                let mycss = document.querySelector('#'+this.$cssid);                 
                if (mycss) {
                    mycss.parentElement.removeChild(mycss);
                }
            }
        });       
        Vue.prototype.$style = '';
        Vue.prototype.$cssid = 'css'+foliadora.next;
     }
}

Vue.use(plugin);

import Ventana from './componente/ventana.js';
import Bandeja from './componente/bandeja.js';
import Player from './componente/player-audio.js';
import Propiedades from './componente/propiedades.js';
import { drop, allowDrop } from './componente/dragdrop.js';

Vue.component('ventana', Ventana);
Vue.component('bandeja', Bandeja);
Vue.component('player', Player);
Vue.component('propiedades', Propiedades);

const app = new Vue({  
    data: {
        titulo: 'VUE'
    },
    methods: {
        drop: function(ev) {
            drop(ev);
        },
        allowDrop: function(ev) {
            allowDrop(ev);
        }
    }
  }).$mount('#app')